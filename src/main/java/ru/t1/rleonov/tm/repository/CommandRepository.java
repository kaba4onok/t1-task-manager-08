package ru.t1.rleonov.tm.repository;

import ru.t1.rleonov.tm.api.ICommandRepository;

import ru.t1.rleonov.tm.constant.TerminalArguments;

import ru.t1.rleonov.tm.constant.TerminalCommands;

import ru.t1.rleonov.tm.model.Command;

public class  CommandRepository implements ICommandRepository {

    public static Command ABOUT = new Command(
            TerminalCommands.ABOUT, TerminalArguments.ABOUT,
            "Show application info."
    );

    public static Command INFO = new Command(
            TerminalCommands.INFO, TerminalArguments.INFO,
            "Show system info."
    );

    public static Command VERSION = new Command(
            TerminalCommands.VERSION, TerminalArguments.VERSION,
            "Show application version."
    );

    public static Command EXIT = new Command(
            TerminalCommands.EXIT, null,
            "Exit application."
    );

    public static Command HELP = new Command(
            TerminalCommands.HELP, TerminalArguments.HELP,
            "Show application commands."
    );

    public static Command COMMANDS = new Command(
            TerminalCommands.COMMANDS, TerminalArguments.COMMANDS,
            "Show application commands."
    );

    public static Command ARGUMENTS = new Command(
            TerminalCommands.ARGUMENTS, TerminalArguments.ARGUMENTS,
            "Show application commands."
    );

    private static Command[] TERMINAL_COMMANDS = new Command[] {
            COMMANDS, ARGUMENTS, ABOUT, INFO, VERSION, HELP, EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    };

}
