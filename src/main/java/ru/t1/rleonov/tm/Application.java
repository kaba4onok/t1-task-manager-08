package ru.t1.rleonov.tm;

import ru.t1.rleonov.tm.api.ICommandRepository;

import ru.t1.rleonov.tm.constant.TerminalArguments;

import ru.t1.rleonov.tm.constant.TerminalCommands;

import ru.t1.rleonov.tm.model.Command;

import ru.t1.rleonov.tm.repository.CommandRepository;

import ru.t1.rleonov.tm.util.FormatUtil;

import java.util.Scanner;

public final class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(final String[] args) {
        if (args == null || args.length == 0) {
            processCommands();
            return;
        }
        processArguments(args);
    }

    private static void processCommands() {
        System.out.println("***WELCOME TO TASK-MANAGER***");
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processCommand(final String command) {
        if (command == null) {
            showErrorCommand();
            return;
        }
        switch (command) {
            case TerminalCommands.ABOUT:
                showAbout();
                break;
            case TerminalCommands.VERSION:
                showVersion();
                break;
            case TerminalCommands.HELP:
                showHelp();
                break;
            case TerminalCommands.INFO:
                showInfo();
                break;
            case TerminalCommands.COMMANDS:
                showCommands();
                break;
            case TerminalCommands.ARGUMENTS:
                showArguments();
                break;
            case TerminalCommands.EXIT:
                exitApplication();
                break;
            default:
                showErrorCommand();
        }
    }

    private static void processArguments(final String[] args) {
        final String arg = args[0];
        processArgument(arg);
    }

    private static void processArgument(final String arg) {
        switch (arg) {
            case TerminalArguments.ABOUT:
                showAbout();
                break;
            case TerminalArguments.VERSION:
                showVersion();
                break;
            case TerminalArguments.HELP:
                showHelp();
                break;
            case TerminalArguments.INFO:
                showInfo();
                break;
            case TerminalArguments.ARGUMENTS:
                showArguments();
                break;
            case TerminalArguments.COMMANDS:
                showCommands();
                break;
            default:
                showErrorArgument();
        }
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Task Manager");
        System.out.println("Developer: Roman Leonov");
        System.out.println("Gitlab: https://gitlab.com/kaba4onok/");
        System.out.println("E-mail: rleonov@t1-consulting.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.8.0");
    }

    private  static  void showInfo() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        System.out.println("Free memory : " + freeMemoryFormat);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = (maxMemoryCheck ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory : " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        System.out.println("Total memory : " + totalMemoryFormat);
        final long usageMemory = totalMemory - freeMemory;
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);
        System.out.println("Usage memory : " + usageMemoryFormat);
    }

    private static void showErrorArgument() {
        System.err.println("Error! This argument is not supported.");
    }

    private static void showErrorCommand() {
        System.err.println("Error! This command is not supported.");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command: commands) System.out.println(command);
    }

    private static void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command: commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    private static void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command: commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    private static void exitApplication() {
        System.exit(0);
    }

}
